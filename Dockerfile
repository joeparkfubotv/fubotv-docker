# A copy of this Dockerfile was used to build an image hosted on Dockerhub
# Dockerhub: 
#   https://hub.docker.com/repository/docker/jpark1120/fubotv
# Current image:
#   node v14.17 - jpark1120/fubotv:web-node-14.17 (current)
# Other circleci available images:
#   https://circleci.com/developer/images/image/cimg/node
# Bitbucket (source repo branch configured in Dockerhub to autobuild): 
#   https://bitbucket.org/g_carey/fubotv-docker/src/node-14.17/Dockerfile
FROM cimg/node:14.17-browsers

USER root

# install python-dev & sendmail
RUN apt-get update && apt-get install -y \
    python-dev \
    sendmail \
    sendmail-bin \
 && rm -rf /var/lib/apt/lists/*

# install pip & awscli
# Legacy python version 2.7 requires us to use this custom pip installer
RUN curl -O https://bootstrap.pypa.io/pip/2.7/get-pip.py && \
    python get-pip.py && \
    rm get-pip.py && \
    pip install awscli pydash

# install static-server for deployments
RUN yarn global add static-server

USER circleci